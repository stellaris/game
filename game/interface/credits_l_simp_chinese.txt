#Stellaris: 3.6 Orion team
--------

##Game Director

Stephen Muray

##Game Designers

Byron Aytoun
Markus Veit

##UX Designer

Matias Laborde
Edouard Choudin

##Content Designers

Gemma Thomson
Hadrien Buytaers
Pierre Du Plessis
Victor Haeggman

##Technical Leads

Johan Melin
Lorenzo Berni

##Programmers

Daniel Bengtsson
Dennis Stockhaus
Dro Tatoian
Kristoffer Sjöö
Olle Sundin
Pierre Du Plessis
Pu Ekstrand

##Release Engineer
Vjola Velikaj

##Art Director

Scott Austin

##Concept & 2D Art

Alec Beals
Felix Englund
Lloyd Drake-Brockman

##UI Art

Cassandra Lindquist
Gabrielle Rodrigues

##3D Art

David Strömblad Lindh
Tim Wiberg

##Animation

Erick Ramirez Mota

##VFX

Erik Forsström

##Technical Artist

Jennifer Larsson-Ståhl

##Embedded QA Lead

Joseph Gardener

##Embedded QA

Daniel Teige
Erik Elgerot
Jaan Lohikoski Håkansson
Malgorzata Grzybowska
Olga Igosheva

##QLOC

## QA Team Leader

Krystian Bas

## Dedicated Testers

Marek Bombala
Maciej Gutkowski
Damian Szokalski
Barbara Wegrzecka

##Audio Director

Ernesto Lopez

##Audio Producer

Sean Scott

##Sound Designer

Robert Wozniak
David Schlein-Andersen 

##Release Manager

Niki Savage

##Product Marketing Manager

Davide Ghelli

##Marketing Creative Producer

Oskar Petrén

##Community Manager

Gareth Kay

##Community Ambassador

Matthew Freake

##External Partners

##Art

N-iX Game & VR Studio


#PDS Green
--------
##Creative Director

Henrik Fåhraeus

##Game Directors

Daniel Moregård
Stephen Muray

##Studio Manager

Rikard Åslund

##Design Director

Kristina Kukyte

##Production Director

Lindsay Kirk

##Technical Director

Niels Uiterwijk

##QA Director

Karl Cederslätt

##Producers

Anton Wittberg Letoff
Aziz Faghihinejad
Loke Norman
Mats Holm
Struan McCallum

##Game Designers

Byron Aytoun
Markus Veit

##UX Designers

Doyle Daigle
Lennart Jönsson
Matias Laborde
Edouard Choudin


##Content Design Lead

Drikus Kuiper

##Content Designers

Chad Inglis
Gemma Thomson
Giada Zavarise
Hadrien Buytaers
Joachim Holmsen
Niamh Calderwood
Pierre Du Plessis
Victor Haeggman

##Programming Manager

Emer Ní Chíobháin

##Technical Leads

Gwenael Tranvouez
Johan Melin
Lorenzo Berni

##Programmers

Alexander Tuzhik
Anton Södergren
Daniel Bengtsson
Dennis Stockhaus
Dro Tatoian
Gustav Palmqvist
Hoda Ismail
Kristoffer Sjöö
Lysann Schlegel
Oleg Istomin
Olle Sundin
Pu Ekstrand
Rose Goullet
Sergey Demidov
Ulrika Uddeborg
Yurii Musiichuk

##Release Engineer
Vjola Velikaj

##Art Manager

Anton Wittberg Letoff

##Art Directors

Fredrik Toll
Scott Austin

##Concept & 2D Art

Alec Beals
Avishek Banerjee
Felix Englund
Lloyd Drake-Brockman
Mattias Larsson

##UI Art

Frenne Hirani
Gabrielle Rodrigues
Cassandra Lindquist
Dmitriy Maksimov

##3D Art

Iniyan Vijay
David Strömblad Lindh
Johan Lundqvist
Pasha Guba
Tim Wiberg

##Animation

Erick Ramirez Mota
Fabian Soon


##VFX

Erik Forsström

##Tech Art

Jennifer Larsson-Ståhl
Woradon Narasetkul

##Embedded QA Lead

Joseph Gardener

##QA Analyst

Marcel Bailleul

##Embedded QA

Daniel Teige
Erik Elgerot
Jaan Lohikoski Håkansson
Malgorzata Grzybowska
Olga Igosheva

##QLOC

## QA Team Leader

Krystian Bas

## Dedicated Testers

Marek Bombala
Maciej Gutkowski
Damian Szokalski
Barbara Wegrzecka

##Audio Director

Ernesto Lopez

##Audio Producer

Sean Scott

##Audio Manager

Franco Freda

##Sound Designer

David Schlein-Andersen
Robert Wozniak

##External Partners

##Art

N-iX Game & VR Studio

##Release Manager

Niki Savage

##Localization Producers

Michael Radnitz
Nick Tsitkou

##Business Owner

Freddy Tu

##Product Marketing Manager

Davide Ghelli

##Performance Marketing Manager

Kimberly Trapnell

##Marketing Creative Producer

Oskar Petrén

##Community Developer

Gareth Kay

##Community Ambassador

Matthew Freake

##Chief Dad Joke Officer

Rikard The Pun-isher

##Body Pillow Coordinator

Mercedes Romero

#Paradox Arctic

##Studio Manager

Carina Holgersson

##Game Director

Petter Nallo

##Producer

Olga Shcherbak

##Game Designers

Joel Hakalax
Karl Emdin
Xuanming Zhou

##Content Designers

Linus Dilén
Marek Koslowski

##UX Designers

Kajsa Olofsson
Mikhail Gofman

##Technical Lead

Johan Melin

##Programmers

Daniel Bengtsson
Emil Lindberg
Erik Hedvall
Marine Baghramyan
Ola Enberg
Olle Sundin

##Embedded QA

Daniel Teige

##Art Leads

Anton Hultdin
Frida Eriksson

##Concept & 2D Art

Anna Windseth
Emma Jonsson

##3D Art

Abraham Gomez

##Animation

Hanna Johansson
#Paradox Interactive
##Management 
 
##Chief Executive Officer
Fredrik Wester

##Chief Finance Officer
Alexander Bricca

##Chief Operations Officer
Charlotta Nilsson

##Chief Business Officer
Johan Bolin

##Chief Creative Officer
Henrik Fåhraeus

##Chief of Staff
Mattias Lilja

##Executive Assistant
Stina Parmenvik 

##Production Team
##Head of Product Launch and Publishing Production
Adrian Irimiea

## Producers
Sebastian Schlyter
Zsolt Suto

##Release Managers
Fang Chen
Kristina Schtollova
Niki Savage
Rebecka Bjurgard

##Compliance Designer
Lars Minkkinen

##Release Coordinator
Rebecca Gelson

##Game Platform Specialist
David Karlsson Lille

##Localization Producers
Michael Radnitz
Nick Tsitkou

##OEM Tech Relations Manager
Jonathan Cole

##QA Manager for Publishing
Jakub Potapczyk

##Business and Marketing

##Vice President of Business
Sandra Neudinger

##Business Owners
Fabricio Santos
Freddy Tu
Luca Kling 
Magnus Lysell
Ryan Sumo

##Product Marketing Management
Alan Mahmodian
Christian Daflos
Davide Ghelli
Jakob Jorstedt
Marine Mazel
Thomas Detaevernier

##Head of Marketing Creative
Adam Skarin

##Marketing Creative Directors
Axel Ullberger
Rachel Oratis

##Marketing Creative Producers
Jennie Nyström
John Ahlbäck
Max Collin
Moushimi Angre
Oskar Petrén
Simon Zickbauer
Yali Murillo

##Head of Digital Marketing
Stefan Wallin

##Paid Media
Hampus Erlandsson
Henrik Bonnichsen
Camilla Wemmenlöv
Kimberly Trapnell
Linn Yngvesson

##Influencer Relations
Jonathan Whitley

##CRM
Annie Alström
Karola Kiessling
Christopher Borén

##PDX WEB
Oscar Gramer
Josef Widoff
João Duduch

##Customer Support
Christian Arvidsson

##Head of Communications
Loïc Fontaine

##Public Relations
Jesse Henning
Troy Goodfellow
Francesca Passoni

##Corporate Communications
Marcus Hallberg
Krzysztof Biernat

##Community Management
Jimmy Tisell
Björn Blomberg
Dale Emasiri
Martyna Zych
Maciej Kozlowski
Troy Pruitt
Benjamin Kajfasz
Preethi Doddy
Debbie Lane
Gareth Kay

##Live Content
Anders Carlsson
Madeleine Jonsson
Alexander Eliasson

##Events Management
Paula Thelin
Veronica Gunlycke
Cissi Ramsby
Umy Yuen Yi Ng


##Research Manager
Stefan Eld

#Researchers
Henrik Edlund
Hanna Löhman
Cherno Jallow
Jenny Wickman
Jeanin Day
Joshua Marchand
Victor Nihlwing

##User Research Administrator
Anna Ström

##Head of Sales
Viktor Stadler

##Partner Account Manager
Albert Banda
Daniel Hjelmtorp
Jackson Downing

##Commercial Manager
Mattias Rengstedt

##Business Development

##VP of Business Development
Daniel Grigorov

##Business Developers 
Mats Karlöf
Nils Brolin
Vivy Zhao

##Executive Producers
Sebastian Forsström
Nils Löf
Gustav Groth
Rostyslav Kaminskyi



##Central Tech & Services

##Central Tech & Services Director
Leif Dellmar

##Head of Data & Analytics
Anabel Silva Rojas

##Cloud Operations Manager
Kim Wahlsten

##Development Manager
Ciaran Kelly

##Chief Architect
Christofer Stegmayr

##Product Owner
Anders Törlind
Mikael Palmér
Erik Boman-Börjesson

##Engine & Tools Engineer
Rickard Lagerbäck
Amanch Esmailzadeh
Andrey Martynyuk
Andy Gainey
Davey van den Berg
Elliot Kim
Flovin Michaelsen
João Galvao
Jonathan Rawet
Kajsa Buckard
Marcus Beausang
Sean Wennström
Trevin Sorenson

##Build & Test Automation Engineer
Mark Dickie
Adel Mubarakshin
Albin Wallin
Alex Smedin
Jakov Denona
Johan Yngman
Kaan Kalenderoglu
Per Hasselström

##Infrastructure & Automation Engineer
Philip Adams
Anton Wermelin
Matthew Hines


##Software Engineer
Alexander Altanis
Ales Bolka
Mikael Berg
William Goh
Daniel Karlsson
Mai-Khanh Lê
Oleg Lindvin
Matthew Price
Jakob Skwarski
Eylem Uğurel
Mikolaj Trzeciecki
Etki Gur
James Gibbs
Vi Lyn Tann

##Data Engineer
Fredrik Hilding
Vlad Caciula

##Data Scientist
Andrew Buckner
Fatemeh Seifan
Stephen White

##BI Specialist
Max Bertilsson

##Game Programming Specialist
Roman Fadeev

##QA Lead
Michaela Ulvhammar
    
##Technical Writer
Johanna Vaher


##IT 

##Head of IT
Thomas Ekholm

##IT Systems Engineer
Mathias Dahlberg

##IT Security Engineer
Thomas Sjögren

##IT Application Manager
Emelie Orraryd

##IT Project Manager
Sarah Lager

##IT Support Team Lead
Richard Lindkvist

##IT Support Technician
Marcus Bose Holmqvist
Robin Ljung Bjuggfält
David Östlund



##Facility & Office

##Facility Management
Malin Hjortenmark

##Office Support Team
Isabella Weski
Oscar Funegård Viberg
Villiam Lindberg


##Finance & Legal

##Finance
Gabriel Andersson
Angelica Halme
Fredrik Sederholm
Sofia Mårtensson
John Rydberg
Elias Gusterman
Kim Danell
Henrik Friis-Liby

##Legal
Fredrik Eklund
Denis Ilecic
Emma Helgesson
Hanna Hansson


##People & Performance

##HR Business Partners
Nathalie Widerberg
Caroline Lagerqvist-Merzan
Ulrika Strandmark

##HR Admin
Natasha Garkusha Sesay

##TA Partners
Erika Stigulfsdotter Holmström
Henrik Tollemark


 
#Stellaris: Toxoids Team
--------
##Game Director

Stephen Muray

##Producers

Fenni Johansson
Loke Norman
Struan McCallum

##Game Designers

Byron Aytoun
Markus Veit

##UX Designer

Matias Laborde

##Content Design Lead

Gemma Thomson

##Content Designers

Chad Inglis
Giada Zavarise
Hadrien Buytaers
Joachim Holmsen
Niamh Calderwood
Pierre Du Plessis
Victor Haeggman

##Technical Leads

Johan Melin
Lorenzo Berni

##Programmers

Daniel Bengtsson
Dennis Stockhaus
Dro Tatoian
Kristoffer Sjöö
Pierre Du Plessis

##Art Director

Scott Austin

##Art Manager
Anton Wittberg Letoff

##Concept & 2D Art

Alec Beals
Anna Windseth
Avishek Banerjee
Emma Jonsson
Felix Englund
Lloyd Drake-Brockman

##UI Art

Nicolas Lennman
Jeremie Tauziede

##3D Art

Abraham Gomez
David Strömblad Lindh
Tim Wiberg

##Animation

Erick Ramirez Mota
Hanna Johansson

##VFX

Erik Forsström

##Technical Artist

Jennifer Larsson-Ståhl

##Embedded QA Lead

Joseph Gardener

##Embedded QA Testers

Daniel Teige
Erik Elgerot
Jaan Lohikoski Håkansson

##QLOC

## QA Team Leader

Adam Nowicki

## Dedicated Testers

Marek Bombala
Maciej Gutkowski
Damian Szokalski
Barbara Wegrzecka

##Audio Director

Ernesto Lopez

##Audio Producer

Sean Scott

##Sound Designer

Gustav Landerholm
David Schlein-Andersen 

##Release Manager

Niki Savage

##Product Marketing Manager

Davide Ghelli

##Marketing Creative Producer

Oskar Petrén

##Community Manager

Gareth Kay

##Community Ambassador

Matthew Freake

##External Partners

##Art

N-iX Game & VR Studio
Raphael Ragimov
#Stellaris: Overlord Team
--------
##Game Directors

Daniel Moregård
Stephen Muray

##Producers

Aziz Faghihinejad
Elin Holmberg
Fenni Johansson

##Game Designers

Byron Aytoun
Guido Schmidt
Markus Veit
Xuanming Zhou

##UX Designers

Doyle Daigle
Sofia Uhr
Valeska Martins

##Content Design Leads

Gemma Thomson
Maximilian Olbers

##Content Designers

Brian Campbell
Chad Inglis
Giada Zavarise
Hadrien Buytaers
Joachim Holmsen
Joshua Evans
Karl Emdin
Pierre Du Plessis
Victor Haeggman

##Technical Leads

Johan Melin
Lorenzo Berni

##Release Engineer

Omar ElGaml

##Programmers

Anton Södergren
Daniel Bengtsson
Dennis Stockhaus
Dro Tatoian
Filip Kirsek
Kristoffer Sjöö
Oleg Istomin
Olle Sundin
Pierre Du Plessis
Rikard Åslund
Rose Goullet
Ulrika Uddeborg
Yurii Musiichuk

##Art Directors

Simon Gunnarsson

##Concept & 2D Art

Alec Beals
Anna Windseth
Avishek Banerjee
Emma Jonsson
Lloyd Drake-Brockman

##UI Art

Nicolas Lennman
Jeremie Tauziede

##3D Art

Abraham Gomez
David Strömblad Lindh
Tim Wiberg

##Animation

Hanna Johansson

##VFX

Erik Forsström

##Technical Artists

Jennifer Larsson-Ståhl

##Embedded QA Lead

Joseph Gardener

##Embedded QA Project Coordinators

Leo Larsson
Struan McCallum

##Embedded QA Testers

Byron Aytoun
Daniel Teige
Erik Elgerot
Eva Mayer
Jaan Lohikoski Håkansson
Sebastian Borg

##QLOC

## QA Team Leader

Adam Nowicki

## Dedicated Testers

Marek Bombala
Maciej Gutkowski
Damian Szokalski
Barbara Węgrzecka

##Audio Producer

Sean Scott

##Sound Designer

Gustav Landerholm

##Release Manager

Oscar Beiroa
Stefana Prozan

##Product Marketing Manager

Pontus Rundqvist
Davide Ghelli

##Marketing Creative Producer

Oskar Petrén

##Community Ambassadors

Graeme Crawford
Matthew Freake

##External Partners

##Art

N-iX Game & VR Studio
Raphael Ragimov

##Community


The Stellaris teams thanks our Japanese Community and especially everyone who worked on the Japanese Language Mod
日本のコミュニティとMODによる日本語化に協力してくれた皆さんに感謝します

#Stellaris: Libra Team
--------
##Creative Director

Henrik Fåhraeus

##Game Director

Daniel Moregård

##Studio Managers

Daniel Lagergren
Rikard Åslund

##Design Director

Kristina Kukyte

##Production Director

Linda Tiger
Mattias Köhlmark

##Technical Director

Niels Uiterwijk

##Studio Art Director

Adrian Banninga

##Producers

Anton Wittberg Letoff
Aziz Faghihinejad
Elin Holmberg
Fenni Johansson
Loke Norman
Loke Wallmo
Olga Shcherbak

##Game Designers

Guido Schmidt
Javier Ansoleaga Martin
Markus Veit
Stephen Muray
Xuanming Zhou

##UX Designers

Doyle Daigle
Lennart Jönsson
Sofia Uhr

##Content Design Leads

Gemma Thomson
Maximilian Olbers

##Content Designers

Brian Campbell
Chad Inglis
Giada Zavarise
Hadrien Buytaers
Henrik Thyrwall
Joachim Holmsen
Joshua Evans
Karl Emdin
Linus Dilén
Pierre Du Plessis
Victor Haeggman

##Programming Manager

Emer Ni Chiobhain

##Technical Leads

Gwenael Tranvouez
Johan Melin
Lorenzo Berni

##Release Engineer

Omar ElGaml

##Programmers

Alexander Tuzhik
Anton Södergren
Daniel Bengtsson
Dennis Stockhaus
Dro Tatoian
Filip Kirsek
Gustav Palmqvist
Kristoffer Sjöö
Lysann Schlegel
Oleg Istomin
Olle Sundin
Rose Goullet
Sergey Demidov
Ulrika Uddeborg
Yurii Musiichuk

##Additional Programming

Pierre du Plessis

##Art Manager

Melinda Esponda Diaz

##Art Directors

Fredrik Toll
Scott Austin
Simon Gunnarsson

##Art Leads

Anton Hultdin
Frida Eriksson

##Concept & 2D Art

Alec Beals
Anna Windseth
Avishek Banerjee
Emma Jonsson
Lloyd Drake-Brockman
Mattias Larsson
Pavel Goloviy

##UI Art

Frenne Hirani
Nicolas Lennman
Jeremie Tauziede

##3D Art

Abraham Gomez
David Strömblad Lindh
Johan Lundqvist
Pasha Guba
Tim Wiberg

##Animation

Erick Mota
Fabian Soon
Hanna Johansson

##VFX

Erik Forsström

##Tech Art

Jennifer Larsson-Ståhl
Woradon Narasetkul

##QA Manager

Karl Cederslätt

##Embedded QA Lead

Joseph Gardener

##Embedded QA Project Coordinators

Leo Larsson
Struan McCallum

##Embedded QA

Byron Aytoun
Daniel Teige
Erik Elgerot
Eva Mayer
Jaan Lohikoski Håkansson
Sebastian Borg

##QLOC

## QA Team Leader

Adam Nowicki

## Dedicated Testers

Marek Bombala
Maciej Gutkowski
Damian Szokalski

##Audio Director

Björn Iversen

##Audio Producer

Sean Scott

##Audio Manager

Franco Freda

##Sound Designer

Gustav Landerholm

##Release Manager

Oscar Beiroa

##Product Manager

Ryan Sumo

##Segment Product Marketing Manager

Filip Sirc

##Product Marketing Manager

Pontus Rundqvist

##Brand Manager

Freddy Tu

##Performance Marketing Manager

Kimberly Trapnell

##Creative Director of Marketing

Axel Ullberger

##Marketing Creative Producer

Oskar Petrén

##Marketing Automation Specialists

Karola Kiessling

##Chief Marketing Officer

Johan Bolin

##Head of Sales

Viktor Stadler

##Partner Account Managers

Albert Banda
Daniel Sinanovic Hjelmtorp
James Gardiner

##Trade Marketing Manager

Jackson Downing

##Commercial Manager

Mattias Rengstedt

##Data Scientist

Eduarda Oliveira
Viktor Stenström

##User Researchers

Victor Nihlwing
Yinyi Luo

##Community Developer

Ann-Charlotte Mörk
Patrycja Gajda

##Influencer Relations Manager

Jonathan Whitley

##Community Ambassadors

Graeme Crawford
Matthew Freake

##Customer Support

Christian Arvidsson
Debbie Lane

##Live Content

Anders Carlsson
Jimmy Tisell
Madeleine Jonsson

##Public Relations

Jesse Henning
Michael Mason
Nick Shepherd

##Web Projects

Franziska Engsner
Josef Widoff
Thea Sorum
Oscar Gramer

##External Partners

##Art

N-iX Game & VR Studio

##Chief Dad Joke Officer

Rikard The Pun-isher

##Body Pillow Coordinator

Mercedes Romero


#Stellaris: Aquatics Team
--------
##Creative Director

Henrik Fåhraeus

##Game Director

Daniel Moregård

##Studio Managers

Daniel Lagergren
Rikard Åslund

##Design Director

Kristina Kukyte

##Production Director

Linda Tiger

##Technical Director

Niels Uiterwijk

##Studio Art Director

Adrian Banninga

##Producers

Anton Wittberg Letoff
Aziz Faghihinejad
Elin Holmberg
Fenni Johansson
Loke Wallmo

##Game Designers

Javier Ansoleaga Martin
Markus Veit
Stephen Muray
Xuanming Zhou

##UX Designers

Doyle Daigle
Sofia Uhr

##Content Design Leads

Gemma Thomson
Maximilian Olbers

##Content Designers

Brian Campbell
Chad Inglis
Giada Zavarise
Guido Schmidt
Hadrien Buytaers
Henrik Thyrwall
Joachim Holmsen
Joshua Evans
Karl Emdin
Linus Dilén
Pierre Du Plessis
Victor Haeggman

##Technical Leads

Gwenael Tranvouez
Lorenzo Berni

##Programmers

Alexander Tuzhik
Anton Södergren
Daniel Bengtsson
Dennis Stockhaus
Dro Tatoian
Filip Kirsek
Gustav Palmqvist
Johan Melin
Kristoffer Sjöö
Lysann Schlegel
Oleg Istomin
Olle Sundin
Rose Goullet
Ulrika Uddeborg
Yurii Musiichuk

##Additional Programming

Pierre du Plessis
Rikard Åslund

##Art Manager

Melinda Esponda Diaz

##Art Directors

Fredrik Toll
Simon Gunnarsson

##Art Leads

Anton Hultdin
Frida Eriksson

##Concept & 2D Art

Anna Windseth
Avishek Banerjee
Emma Jonsson
Lloyd Drake-Brockman
Mattias Larsson
Pavel Goloviy

##UI Art

Frenne Hirani
Nicolas Lennman
Jeremie Tauziede

##3D Art

Abraham Gomez
David Strömblad Lindh
Johan Lundqvist
Pasha Guba
Tim Wiberg

##Animation

Fabian Soon
Hanna Johansson

##VFX

Erik Forsström

##QA Manager

Karl Cederslätt

##Embedded QA Lead

Joseph Gardener

##Embedded QA Project Coordinators

Leo Larsson
Struan McCallum

##Embedded QA

Byron Aytoun
Daniel Teige
Erik Elgerot
Eva Mayer
Jaan Lohikoski Håkansson
Sebastian Borg

##QLOC

## QA Team Leader

Adam Nowicki

## Dedicated testers

Marek Bombala
Maciej Gutkowski
Damian Szokalski

##Audio Director

Björn Iversen

##Audio Producer

Sean Scott

##Audio Manager

Franco Freda

##Sound Designers

Gustav Landerholm

##External Partners

##Art

N-iX Game & VR Studio

##Release Manager

Oscar Beiroa

##Product Manager

Anna Golyakova
Ryan Sumo

##Segment Product Marketing Manager

Filip Sirc

##Product Marketing Manager

Pontus Rundqvist

##Brand Manager

Freddy Tu

##Performance Marketing Manager

Kimberly Trapnell

##Creative Director of Marketing

Axel Ullberger

##Marketing Creative Producer

Oskar Petrén

##Marketing Automation Specialists

Annie Alström
Karola Kiessling

##Chief Marketing Officer

Johan Bolin

##Manager of Partner Relations

Viktor Stadler

##Partner Account Managers

Albert Banda
Daniel Hjelmtorp
Ebba Frantzén
James Gardiner

##Trade Marketing Manager

Jackson Downing

##Commercial Manager

Mattias Rengstedt
Sofie Siggelin

##Data Scientist

Eduarda Oliveira
Stephen White

##User Researchers

Victor Nihlwing
Yinyi Luo

##Community Developer

Ann-Charlotte Mörk

##Influencer Relations Manager

Jonathan Whitley

##Community Ambassadors

Graeme Crawford
Matthew Freake

##Customer Support

Christian Arvidsson
Debbie Lane

##Live Content

Jimmy Tisell
Anders Carlsson
Madeleine Jonsson

##Public Relations

Jesse Henning
Nick Shepherd
Michael Mason

##Chief Dad Joke Officer

Rikard The Pun-isher

##Body Pillow Coordinator

Mercedes Romero





#Stellaris: Stanislaw Lem Team
--------
##Creative Director

Henrik Fåhraeus

##Game Director

Daniel Moregård

##Producer

Aziz Faghihinejad
Daniel Lagergren
Elin Holmberg

##Designers

Guido Schmidt
Javier Ansoleaga Martin
Markus Veit
Stephen Muray
Xuanming Zhou

##Additional Design

Byron Aytoun

##Technical Director

Niels Uiterwijk

##Technical Lead

Lorenzo Berni

##Programmers

Anton Södergren
Daniel Bengtsson
Filip Kiršek
Johan Melin
Klas Linde
Oleg Istomin
Roger Weber
Tobias Luckey
Ulrika Uddeborg
Yurii Musiichuk

##Additional Programming

Pierre du Plessis

##Content Design Leads

Gemma Thomson
Maximilian Olbers

##Content Designers

Hadrien Buytaers
Joachim Holmsen
Joshua Evans
Karl Emdin
Linus Dilén
Pierre du Plessis
Victor Haeggman

##Additional Content Design

Byron Aytoun

##UX Designers

Sofia Uhr
Valeska Martins

##Art Producers

Anton Wittberg Letoff
Fenni Johansson

##Art Director

Simon Gunnarsson


##Art Leads

Anton Hultdin
Frida Eriksson

##Concept & 2D Art

Anna Windseth
Emma Jonsson
Mattias Larsson
Pavel Goloviy
Ylva Ljungqvist

##UI Art

Nicolas Lennman
Jeremie Tauziede

##3D Art

Abraham Gomez
David Strömblad Lindh
Johan Lundqvist
Tim Wiberg

##Animation

Hanna Johansson

##VFX

Erik Forsström

##Audio Director

Björn Iversen

##Audio Producer

Sean Scott

##Sound Designers

Franco Freda
Gustav Landerholm
Tobey Evans

##External Partners

##Art

N-iX Game & VR Studio

##Embedded QA Lead

Joseph Gardener

##Embedded QA Project Coordinators

Leo Larsson
Struan McCallum

##Embedded QA

Byron Aytoun
Daniel Teige
Erik Elgerot
Jaan Lohikoski Håkansson
Sebastian Borg

##QA Central Team Lead

Filippa Gannholm Kirsten

##QLOC

## QA Team Leader

Adam Nowicki

## Dedicated testers

Marek Bombała
Maciej Gutkowski
Damian Szokalski

##QA Testers (additional)

Grzegorz Nowick

##Keywords Studios Katowice

##FQA Project Manager

Marcin Jankowski

##FQA Associate Lead

Marcin Sebesta

##FQA Senior Tester

Dawid Kolaczuch

##FQA Testers

Mikolaj Bizacki
Krzysztof Bochenek
Kamil Drezek
Dawid Kabasinski
Rafal Kaluza
Natalia Lechnata
Mateusz Mierzwa
Konrad Ruminski

##Beta Testers

Francesco Teruzzi
Jonathan Bagley
Joshua "Mordaith" Smith
Lumi Schildkraut
Maik "Malthus" Wrba
Marc Schmelzer
Matthew Walsh
Michael De Bonaoscar
Nina Chance
Sergey Demidov

##Release Manager

Oscar Beiroa

##Product Manager

Anna Golyakova
Simon Stackmann

##Segment Product Marketing Manager

Filip Sirc

##Product Marketing Manager

Pontus Rundqvist

##Brand Manager

Freddy Tu

##Performance Marketing Manager

Kimberly Trapnell
Michiel Van Loo

##Creative Director of Marketing

Axel Ullberger

##Marketing Creative Producer

Oskar Petrén

##Marketing Automation Specialists

Johanna Kasurinen
Karola Kiessling
Annie Alström

##Chief Marketing Officer

Johan Bolin

##Manager of Partner Relations

Viktor Stadler

##Partner Account Managers

James Gardiner
Albert Banda
Ebba Frantzén
Daniel Hjelmtorp

##Trade Marketing Manager

Jackson Downing

##Commercial Manager

Sofie Siggelin
Mattias Rengstedt

##Data Scientist

Stephen White
Eduarda Oliveira

##User Researchers

Yinyi Luo

##Community Developer

Ann-Charlotte Mörk

##Influencer Relations Manager
Jonathan Whitley

##Community Ambassador

Graeme Crawford
Matthew Freake

##Customer Support

Christian Arvidsson
Debbie Lane

##Live Content

Jimmy Tisell
Anders Carlsson
Madeleine Jonsson

##Public Relations

Jesse Henning
Nick Shepherd
Michael Mason

##Chief Dad Joke Officer

Rikard The Pun-isher

##Body Pillow Coordinator

Mercedes Romero



#Stellaris: Nemesis Game Team
--------
##Creative Director

Henrik Fåhraeus

##Game Director

Daniel Moregård

##Producer

Aziz Faghihinejad
Daniel Lagergren
Elin Holmberg

##Designer

Markus Veit
Stephen Muray

##Technical Leads

Gwenael Tranvouez
Lorenzo Berni
Rickard Lagerbäck

##Programmers

Anton Södergren
Filip Kiršek
Gustav Palmqvist
Ilya Nikitin
Johan Melin
Jörgen Risholt
Mathieu Ropert
Oleg Istomin
Tobias Luckey
Ulrika Uddeborg

##Additional Programming

Pierre du Plessis

##Content Design Leads

Henrik Thyrwall
Maximilian Olbers

##Content Designers

Douglas Furén
Gemma Thomson
Pierre du Plessis

##UX Designer

Doyle Merlin Daigle II
Valeska Martins

##Art Producers

Anton Wittberg Letoff
Melinda Esponda Díaz

##Art Lead

Simon Gunnarsson

##Concept Art

Mattias Larsson
Ylva Ljungqvist
Ecbel Oueslati
Alessandro Bragalini

##UI Art

Nicolas Lennman
Jeremie Tauziede

##3D Art

Johan Lundqvist

##Animation

Fabian Soon
Hanna Johansson

##VFX

Erik Forsström

##Audio Director

Björn Iversen

##Sound Designers

David Schlein-Andersen
Franco Freda
Tobey Evans

##External Partners

##Art

N-iX Game & VR Studio
Game Ace Studio

##Embedded QA Lead

Joseph Gardener

##Embedded QA Project Coordinator

Fenni Johansson
Rebecca Romin

##Embedded QA

Byron Aytoun
Daniel Teige
Erik Elgerot
Eva Mayer
Struan McCallum

##QA Central Team Lead

Filippa Gannholm Kirsten

##QLOC

## QA Team Leader

Adam Nowicki

## Dedicated testers

Marek Bombała
Maciej Gutkowski
Damian Szokalski

##QA Testers (additional)

Grzegorz Nowick

##Keywords Studios Katowice

##FQA Project Manager

Marcin Jankowski

##FQA Associate Lead

Marcin Sebesta

##FQA Senior Tester

Dawid Kolaczuch

##FQA Testers

Mikolaj Bizacki
Krzysztof Bochenek
Kamil Drezek
Dawid Kabasinski
Rafal Kaluza
Natalia Lechnata
Mateusz Mierzwa
Konrad Ruminski

##Beta Testers

Francesco Teruzzi
Jonathan Bagley
Joshua "Mordaith" Smith
Lumi Schildkraut
Maik "Malthus" Wrba
Marc Schmelzer
Matthew Walsh
Michael De Bona
Nina Chance
Sergey Demidov

##Product Manager

Simon Stackmann

##Product Marketing Manager

Pontus Rundqvist

##Performance Marketing Manager

Michiel Van Loo

##Creative Director of Marketing

Axel Ullberger

##Marketing Creative Producer

Oskar Petrén

##Marketing Automation Specialists

Johanna Kasurinen
Karola Kiessling
Annie Alström

##Vice President Sales

Johan Bolin

##Acting Team Lead - Partner Relations

Viktor Stadler

##Partner Account Managers

James Gardiner

Albert Banda

Ebba Frantzén

Daniel Hjelmtorp

##Trade Marketing Manager

Jackson Downing

##Commercial Manager

Sofie Siggelin

##Data Scientist

Stephen White
Eduarda Oliveira

##User Researchers

Yinyi Luo

##Community Developer

Ann-Charlotte Mörk

##Influencer Relations Manager
Jonathan Whitley

##Community Ambassador

Graeme Crawford
Matthew Freake

##Customer Support

Christian Arvidsson
Debbie Lane

##Live Content

Jimmy Tisell
Anders Carlsson

##Public Relations

Nick Shepherd
Michael Mason

##Chief Dad Joke Officer

Rikard The Pun-isher

##Body Pillow Coordinator

Mercedes Romero

# Licenses
--------
Stellaris uses the following third party libraries.
See the accompanying license files for more details

Breakpad Copyright (c) 2006, Google Inc.
Copyright 2001-2004 Unicode, Inc.
Copyright (c) 1999 Apple Computer, Inc.
Copyright (c) 1989, 1993 The Regents of the University of California.

Boost Copyright (c) 2003 Boost Software License

bzip2 Copyright (C) 1996-2010 Julian R Seward.

curl Copyright (c) 1996 - 2011, Daniel Stenberg, <daniel@haxx.se>.

Dear ImGui Copyright (c) 2014-2021 Omar Cornut

Eigen is used under the Mozilla Public License Version 2.0

FreeType Copyright 1996-2002, 2006 by David Turner, Robert Wilhelm, and Werner Lemberg

FreeImage open source image library. See http://freeimage.sourceforge.net for details. FreeImage is used under the FIPL, version 1.0. 

The OpenGL Extension Wrangler Library
Copyright (C) 2008-2015, Nigel Stewart <nigels[]users sourceforge net>
Copyright (C) 2002-2008, Milan Ikits <milan ikits[]ieee org>
Copyright (C) 2002-2008, Marcelo E. Magallon <mmagallo[]debian org>
Copyright (C) 2002, Lev Povalahev

Mesa 3-D graphics library 7.0 Copyright (c) 1999-2007 Brian Paul

GLEW Copyright (c) 2007 The Khronos Group Inc.

ImPlot Copyright (c) 2020 Evan Pezent

libogg Copyright (c) 2002, Xiph.org Foundation

libwebm Copyright (c) 2010, Google Inc.

Nakama C/C++ Client SDK Copyright 2021 Heroic Labs

XPlatCppSdk Copyright (C) 2022, Microsoft Corporation.
Portions of this program (c) 2020 Microsoft Azure PlayFab

precise_fixpoint Copyright (c) 2007-2009: Peter Schregle

Simple DirectMedia Layer Copyright (C) 1997-2022 Sam Lantinga <slouken@libsdl.org>

Stackwalker Copyright (c) 2005-2013, Jochen Kalmbach

Ogg Vorbis Copyright (c) 2002-2008 Xiph.org Foundation

PhysFS Copyright (c) 2001-2019 Ryan C. Gordon and others.

zlib Copyright (C) 1995-2004 Jean-loup Gailly and Mark Adler

Malgun Gothic Font Family © 2016 Microsoft Corporation. All Rights Reserved.
Typeface © The Monotype Corporation plc. Data © The Monotype Corporation plc / Type Solutions Inc. 1990-91 All Rights Reserved
Digitized data copyright Monotype Typography, Ltd 1991-1995. All rights reserved. 
--------
