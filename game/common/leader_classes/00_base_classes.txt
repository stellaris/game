@leader_unity_upkeep = 2

admiral = {
	can_lead_navy = yes
	max_trait_points = 1
	can_have_traits = yes
	resources = {
		category = leader_admirals
		cost = {
			unity = 1
			multiplier = owner.value:leader_cost
		}
		upkeep = {
			trigger = {
				has_leader_upkeep = yes
			}
			unity = @leader_unity_upkeep
		}
	}

	default_category = admiral
}

general = {
	can_lead_army = yes
	max_trait_points = 1
	can_have_traits = yes
	resources = {
		category = leader_generals
		cost = {
			unity = 1
			multiplier = owner.value:leader_cost
		}
		upkeep = {
			trigger = {
				has_leader_upkeep = yes
			}
			unity = @leader_unity_upkeep
		}
	}

	default_category = general
}

scientist = {
	can_research = yes
	max_trait_points = 1
	can_have_traits = yes
	resources = {
		category = leader_scientists
		cost = {
			unity = 1
			multiplier = owner.value:leader_cost
		}
		upkeep = {
			trigger = {
				has_leader_upkeep = yes
			}
			unity = @leader_unity_upkeep
		}
		produces = {
			trigger = {
				has_trait = leader_trait_scientist_composer_chosen
			}
			volatile_motes = 2
			exotic_gases = 2
			rare_crystals = 2
		}
		produces = {
			trigger = {
				has_trait = leader_trait_scientist_instrument_chosen
			}
			sr_zro = 2
		}
	}
	default_category = scientist
}

governor = {
	can_govern_planet = yes
	max_trait_points = 1
	can_have_traits = yes
	resources = {
		category = leader_governors
		cost = {
			unity = 1
			multiplier = owner.value:leader_cost
		}
		upkeep = {
			trigger = {
				has_leader_upkeep = yes
			}
			unity = @leader_unity_upkeep
		}
		produces = {
			trigger = {
				is_idle = no
				exists = owner
				owner = {
					has_valid_civic = civic_feudal_realm
				}
			}
			unity = 1
			multiplier = trigger:has_skill
		}
	}
	default_category = governor
}

ruler = {
	can_rule_empire = yes
	max_trait_points = 2
	can_have_traits = yes
	default_category = ruler
}

envoy = {
	can_be_envoy = yes
	default_category = envoy
	assignment_cooldown = 360
	can_have_traits = no
	resources = {
		produces = {
			trigger = {
				is_idle = no
				exists = owner
				owner = {
					has_tradition = tr_politics_adopt
				}
				OR = {
					has_envoy_task = {
						task = galactic_community
					}
					has_envoy_task = {
						task = strengthen_imperial_authority
					}
					has_envoy_task = {
						task = undermine_imperial_authority
					}
				}
			}
			influence = 0.1
		}
	}
}
